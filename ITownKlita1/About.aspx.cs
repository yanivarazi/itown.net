﻿using ITown.MS.Graph;
using Microsoft.Graph;
using System.Configuration;
using System.IO;
using System.Web.UI;

namespace ITownKlita1
{
    public partial class About : Page
    {
        
        /// <summary>
        /// 
        /// </summary>
        public string ExcelUrl
        {
            get
            {
                try
                {                     
                    string user = ConfigurationManager.AppSettings["itown.onedrive.user"].ToString();
                    string password = ConfigurationManager.AppSettings["itown.onedrive.password"].ToString();
                    string onedrivePath = ConfigurationManager.AppSettings["itown.onedrive.path"].ToString();
                    string serverDrive = ConfigurationManager.AppSettings["itown.onedrive.drive"].ToString();
                    string sOneDrivePath = string.Format("{0}\\{1}", serverDrive, onedrivePath);
                    sOneDrivePath = sOneDrivePath.Replace("\\\\", "\\");
                    

                    //
                    OneDriveFile odf = new OneDriveFile(user, password);

                    string di = odf.SharingLink(onedrivePath);
                    return di;
                    //DriveItem di = odf.Upload(@"C:\Users\yaniv\Documents\יניב.docx").Result;
                    string drivePath = ConfigurationManager.AppSettings["onedrive.path"];
                    //(@"C:\Users\yaniv\Downloads\ITownKlita_Sheet1.xlsx");    
                    if (!Directory.Exists(drivePath))
                    {
                        Directory.CreateDirectory(drivePath);
                    }
                    string sourcePath = @"C:\Users\yaniv\Documents\ITownInfoPageWeb.zip";
                    string[] arr = sourcePath.Split("\\".ToCharArray());
                    string fileName = arr[arr.Length - 1];
                    string destinationPath = string.Format("{0}\\{1}", drivePath, fileName);
                    //
                    System.IO.File.Copy(sourcePath, destinationPath);
                    
                    //return System.Configuration.ConfigurationManager.AppSettings["itown.data.excel.url"].ToString();
                    return "about:blank";
                }
                catch
                {
                    return null;
                }
            }
        }

    }
}