﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ITownKlita1._Default" %>

<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server" >



    <div class="row" dir="rtl">
        <div >
            <h2>קליטה</h2>
            <asp:Panel ID="Panel1" runat="server" Height="54px" HorizontalAlign="Center" style="text-align: right">
                <asp:TextBox ID="txtWhere" runat="server" CssClass="itownTextSearch" Height="40px" Width="753px" ></asp:TextBox>
                <asp:ImageButton ID="ImageButton1" runat="server" CssClass="itownCommandRefreshSearch" height="35px" ImageAlign="Middle" ImageUrl="https://framework.intertown.co.il/i9205/intertown/dijit/css/images/right/panel/icon/itown.panel.right.icon.search.png" OnClick="ImageButton1_Click" Width="35px" />
            </asp:Panel>
            <p>
                <asp:DetailsView   ID="DetailsView1" runat="server" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal" Height="50px" Width="829px" AutoGenerateRows="False" DataKeyNames="OID" DataSourceID="itownklita" AllowPaging="True" ValidateRequestMode="Enabled"  >
                    <AlternatingRowStyle BackColor="#F7F7F7" />
                    <EditRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                    <Fields>
                        <asp:BoundField DataField="OID" HeaderText="OID" ReadOnly="True" SortExpression="OID" InsertVisible="False"  />
                        <asp:BoundField DataField="check_v" HeaderText="check_v" SortExpression="check_v"    />
                        <asp:BoundField DataField="code_mavaat" HeaderText="code_mavaat" SortExpression="code_mavaat" />
                        <asp:BoundField DataField="land_desgnation_plan" HeaderText="land_desgnation_plan" SortExpression="land_desgnation_plan" />
                        <asp:BoundField DataField="code_taba" HeaderText="code_taba" SortExpression="code_taba" />
                        <asp:BoundField DataField="plan_date" HeaderText="plan_date" SortExpression="plan_date" />
                        <asp:BoundField DataField="lot_num" HeaderText="lot_num" SortExpression="lot_num" />
                        <asp:BoundField DataField="yeshuv_name" HeaderText="yeshuv_name" SortExpression="yeshuv_name" />
                        <asp:BoundField DataField="lot_area" HeaderText="lot_area" SortExpression="lot_area" />
                        <asp:BoundField DataField="uses" HeaderText="uses" SortExpression="uses" />
                        <asp:BoundField DataField="bld_area_above_main" HeaderText="bld_area_above_main" SortExpression="bld_area_above_main" />
                        <asp:BoundField DataField="bld_area_above_service" HeaderText="bld_area_above_service" SortExpression="bld_area_above_service" />
                        <asp:BoundField DataField="bld_area_below_main" HeaderText="bld_area_below_main" SortExpression="bld_area_below_main" />
                        <asp:BoundField DataField="bld_area_below_service" HeaderText="bld_area_below_service" SortExpression="bld_area_below_service" />
                        <asp:BoundField DataField="bld_area_total_m2" HeaderText="bld_area_total_m2" SortExpression="bld_area_total_m2" />
                        <asp:BoundField DataField="bld_area_total_perc" HeaderText="bld_area_total_perc" SortExpression="bld_area_total_perc" />
                        <asp:BoundField DataField="bld_line_frnt" HeaderText="bld_line_frnt" SortExpression="bld_line_frnt" />
                        <asp:BoundField DataField="bld_line_rear" HeaderText="bld_line_rear" SortExpression="bld_line_rear" />
                        <asp:BoundField DataField="bld_line_dirc_side_one" HeaderText="bld_line_dirc_side_one" SortExpression="bld_line_dirc_side_one" />
                        <asp:BoundField DataField="bld_line_dirc_side_two" HeaderText="bld_line_dirc_side_two" SortExpression="bld_line_dirc_side_two" />
                        <asp:BoundField DataField="building_height" HeaderText="building_height" SortExpression="building_height" />
                        <asp:BoundField DataField="floors" HeaderText="floors" SortExpression="floors" />
                        <asp:BoundField DataField="basements" HeaderText="basements" SortExpression="basements" />
                        <asp:BoundField DataField="apartments" HeaderText="apartments" SortExpression="apartments" />
                        <asp:BoundField DataField="density" HeaderText="density" SortExpression="density"  />
                        <asp:BoundField DataField="land_cover" HeaderText="land_cover" SortExpression="land_cover" />
                        
                        <asp:BoundField DataField="other_limitations" HeaderText="other_limitations" SortExpression="other_limitations" />
                        <asp:BoundField DataField="another_comment" HeaderText="another_comment" SortExpression="another_comment" />
                        <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowInsertButton="True" />
                    </Fields>

                  
                    <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                    <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                    <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                    <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C"  />

                  
                </asp:DetailsView>
 
                <asp:SqlDataSource  ID="itownklita" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:ITown_KlitaConnectionString %>" 
                    DeleteCommand="DELETE FROM [Sheet1] WHERE [OID] = @original_OID" 
                    InsertCommand="INSERT INTO [Sheet1] ([check_v], [code_mavaat], [land_desgnation_plan], [code_taba], [plan_date], [lot_num], [yeshuv_name], [lot_area], [uses], [bld_area_above_main], [bld_area_above_service], [bld_area_below_main], [bld_area_below_service], [bld_area_total_m2], [bld_area_total_perc], [bld_line_frnt], [bld_line_rear], [bld_line_dirc_side_one], [bld_line_dirc_side_two], [building_height], [floors], [basements], [apartments], [density], [land_cover], [other_limitations], [another_comment]) VALUES (@check_v, @code_mavaat, @land_desgnation_plan, @code_taba, @plan_date, @lot_num, @yeshuv_name, @lot_area, @uses, @bld_area_above_main, @bld_area_above_service, @bld_area_below_main, @bld_area_below_service, @bld_area_total_m2, @bld_area_total_perc, @bld_line_frnt, @bld_line_rear, @bld_line_dirc_side_one, @bld_line_dirc_side_two, @building_height, @floors, @basements, @apartments, @density, @land_cover, @other_limitations, @another_comment)" 
                    SelectCommand="SELECT * FROM [Sheet1]" 
                    UpdateCommand="UPDATE [Sheet1] SET [check_v] = @check_v, [code_mavaat] = @code_mavaat, [land_desgnation_plan] = @land_desgnation_plan, [code_taba] = @code_taba, [plan_date] = @plan_date, [lot_num] = @lot_num, [yeshuv_name] = @yeshuv_name, [lot_area] = @lot_area, [uses] = @uses, [bld_area_above_main] = @bld_area_above_main, [bld_area_above_service] = @bld_area_above_service, [bld_area_below_main] = @bld_area_below_main, [bld_area_below_service] = @bld_area_below_service, [bld_area_total_m2] = @bld_area_total_m2, [bld_area_total_perc] = @bld_area_total_perc, [bld_line_frnt] = @bld_line_frnt, [bld_line_rear] = @bld_line_rear, [bld_line_dirc_side_one] = @bld_line_dirc_side_one, [bld_line_dirc_side_two] = @bld_line_dirc_side_two, [building_height] = @building_height, [floors] = @floors, [basements] = @basements, [apartments] = @apartments, [density] = @density, [land_cover] = @land_cover, [other_limitations] = @other_limitations, [another_comment] = @another_comment WHERE [OID] = @original_OID" 
                    OldValuesParameterFormatString="original_{0}" >
                    <DeleteParameters>
                        <asp:Parameter Name="original_OID" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="check_v" Type="String" />
                        <asp:Parameter Name="code_mavaat" Type="String" />
                        <asp:Parameter Name="land_desgnation_plan" Type="String" />
                        <asp:Parameter Name="code_taba" Type="String" />
                        <asp:Parameter Name="plan_date" Type="DateTime" />
                        <asp:Parameter Name="lot_num" Type="String" />
                        <asp:Parameter Name="yeshuv_name" Type="String" />
                        <asp:Parameter Name="lot_area" Type="String" />
                        <asp:Parameter Name="uses" Type="String" />
                        <asp:Parameter Name="bld_area_above_main" Type="String" />
                        <asp:Parameter Name="bld_area_above_service" Type="String" />
                        <asp:Parameter Name="bld_area_below_main" Type="String" />
                        <asp:Parameter Name="bld_area_below_service" Type="String" />
                        <asp:Parameter Name="bld_area_total_m2" Type="String" />
                        <asp:Parameter Name="bld_area_total_perc" Type="String" />
                        <asp:Parameter Name="bld_line_frnt" Type="String" />
                        <asp:Parameter Name="bld_line_rear" Type="String" />
                        <asp:Parameter Name="bld_line_dirc_side_one" Type="String" />
                        <asp:Parameter Name="bld_line_dirc_side_two" Type="String" />
                        <asp:Parameter Name="building_height" Type="String" />
                        <asp:Parameter Name="floors" Type="Double" />
                        <asp:Parameter Name="basements" Type="String" />
                        <asp:Parameter Name="apartments" Type="String" />
                        <asp:Parameter Name="density" Type="String" />
                        <asp:Parameter Name="land_cover" Type="Double" />
                        <asp:Parameter Name="other_limitations" Type="String" />
                        <asp:Parameter Name="another_comment" Type="String" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="check_v" Type="String" />
                        <asp:Parameter Name="code_mavaat" Type="String" />
                        <asp:Parameter Name="land_desgnation_plan" Type="String" />
                        <asp:Parameter Name="code_taba" Type="String" />
                        <asp:Parameter Name="plan_date" Type="DateTime" />
                        <asp:Parameter Name="lot_num" Type="String" />
                        <asp:Parameter Name="yeshuv_name" Type="String" />
                        <asp:Parameter Name="lot_area" Type="String" />
                        <asp:Parameter Name="uses" Type="String" />
                        <asp:Parameter Name="bld_area_above_main" Type="String" />
                        <asp:Parameter Name="bld_area_above_service" Type="String" />
                        <asp:Parameter Name="bld_area_below_main" Type="String" />
                        <asp:Parameter Name="bld_area_below_service" Type="String" />
                        <asp:Parameter Name="bld_area_total_m2" Type="String" />
                        <asp:Parameter Name="bld_area_total_perc" Type="String" />
                        <asp:Parameter Name="bld_line_frnt" Type="String" />
                        <asp:Parameter Name="bld_line_rear" Type="String" />
                        <asp:Parameter Name="bld_line_dirc_side_one" Type="String" />
                        <asp:Parameter Name="bld_line_dirc_side_two" Type="String" />
                        <asp:Parameter Name="building_height" Type="String" />
                        <asp:Parameter Name="floors" Type="Double" />
                        <asp:Parameter Name="basements" Type="String" />
                        <asp:Parameter Name="apartments" Type="String" />
                        <asp:Parameter Name="density" Type="String" />
                        <asp:Parameter Name="land_cover" Type="Double" />
                        <asp:Parameter Name="other_limitations" Type="String" />
                        <asp:Parameter Name="another_comment" Type="String" />
                        <asp:Parameter Name="original_OID" Type="Int32" />
                    </UpdateParameters>
          
     
  
                </asp:SqlDataSource>
 
            </p>
 
        </div>


    </div>

  

</asp:Content>
