﻿

using System.Net.Http.Headers;
using Microsoft.Graph;

namespace ITown.MS.Graph.Helpers
{
    public class SDKHelper
    {
        /// <summary>
        /// 
        /// </summary>
        private static GraphServiceClient graphClient = null;

        /// <summary>
        /// Get an authenticated Microsoft Graph Service client.
        /// </summary>
        /// <param name="_userName"></param>
        /// <param name="_password"></param>
        /// <returns></returns>
        public static GraphServiceClient GetAuthenticatedClient(string _userName, string _password )
        {
            if (graphClient != null) { return graphClient;  }
            //
            graphClient = new GraphServiceClient(
                new DelegateAuthenticationProvider(
                    async (requestMessage) =>
                    {
                        //
                        string accessToken = (Token.Get(_userName, _password)).AccessToken;
                        //
                        // Append the access token to the request.
                        requestMessage.Headers.Authorization = new AuthenticationHeaderValue("bearer", accessToken);
                        //
                        // This header has been added to identify our sample in the Microsoft Graph service. If extracting this code for your project please remove.
                        //requestMessage.Headers.Add("SampleID", "aspnet-connect-sample");                        
                        
                    }));
            //                        
            return graphClient;
            //
        }
        /// <summary>
        /// 
        /// </summary>
        public static void SignOutClient()
        {

            graphClient = null;
        }
    }
}