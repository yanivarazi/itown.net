﻿using Newtonsoft.Json.Linq;
using System;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;

namespace ITown.MS.Graph
{
    public class Token
    {
        private static string clientId = ConfigurationManager.AppSettings["ida:AppId"];
        private static string clientSecret = ConfigurationManager.AppSettings["ida:AppSecret"];
        private static string redirectUri = ConfigurationManager.AppSettings["ida:RedirectUri"];
        private static string graphScopes = ConfigurationManager.AppSettings["ida:GraphScopes"];
        /// <summary>
        /// 
        /// </summary>
        private static Token _token = null;
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static Token Get(string _userName = null, string _password = null)
        {
            try
            {
                if (_token == null)
                {
                    _token = new Token(_userName, _password);
                }
                return _token;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        //
        private string userName = null;
        private string password = null;
		/// <summary>
        /// 
        /// </summary>
        private string accessToken = null;
        //
        /// <summary>
        /// Get an access token to use for testing the sample calls.
        /// </summary>
        private void GetAccessTokenUsingPasswordGrant()
        {
            
            //
            JObject jResult = null;
            String urlParameters = String.Format(
                    "grant_type={0}&resource={1}&client_id={2}&client_secret={3}&username={4}&password={5}",
                    "password",
                    "https%3A%2F%2Fgraph.microsoft.com%2F",
                    clientId,
                    clientSecret,
                    userName,
                    password
            );

            HttpClient client = new HttpClient();
            var body = new StringContent(urlParameters, System.Text.Encoding.UTF8, "application/x-www-form-urlencoded");
            Task<HttpResponseMessage> requestTask = client.PostAsync("https://login.microsoftonline.com/common/oauth2/token", body);
            requestTask.Wait();
            HttpResponseMessage response = requestTask.Result;

            if (response.IsSuccessStatusCode)
            {
                Task<string> responseTask = response.Content.ReadAsStringAsync();
                responseTask.Wait();
                string responseContent = responseTask.Result;
                jResult = JObject.Parse(responseContent);
                AccessToken = (string)jResult["access_token"];
            }
        }
		///
        private Token( string _userName, string _password)
        {
            this.userName = _userName;
            this.password = _password;
            //
            GetAccessTokenUsingPasswordGrant();
        }
		/// <summary>
        /// 
        /// </summary>
        public  string AccessToken
        {
            get
            {
                return this.accessToken;
            }
			set
            {
                this.accessToken = value;
            }
        }

    }
}