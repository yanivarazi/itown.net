﻿using ITown.MS.Onedrive.Rest.Models;
using Microsoft.Graph;
using Newtonsoft.Json;
using System;

using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ITown.MS.Graph
{
    class OneDriveFile
    {
        
        /// <summary>
        /// 
        /// </summary>
        private string userName = null;
        private string password = null;
        private GraphServiceClient gClient = null;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sLocalPath"></param>
        /// <param name="sRemoteName"></param>
        /// <param name="_userName"></param>
        /// <param name="_password"></param>
        public OneDriveFile(string _userName, string _password)
        {
            userName = _userName;
            password = _password;
            Token.Get(userName, password);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sLocalPath"></param>
        /// <returns></returns>
        public async Task<Onedrive.Rest.Models.FileInfo> Upload(System.IO.Stream file)
        {
            // This operation only supports files up to 4MB in size.
            // To upload larger files, see `https://developer.microsoft.com/graph/docs/api-reference/v1.0/api/item_createUploadSession`.
            string endpoint = "https://graph.microsoft.com/v1.0/me/drive/root/children/mypic.jpg/content";

            using (var client = new HttpClient())
            {
                using (var request = new HttpRequestMessage(HttpMethod.Put, endpoint))
                {
                    request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", Token.Get().AccessToken);
                    request.Content = new StreamContent(file);
                    request.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpg");
                    using (var response = await client.SendAsync(request))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            string stringResult = await response.Content.ReadAsStringAsync();
                            return JsonConvert.DeserializeObject<Onedrive.Rest.Models.FileInfo>(stringResult);
                        }
                        else return null;
                    }
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sLocalPath"></param>
        /// <returns></returns>
        public async Task<FileInfo> info(string filePath)
        {
            //
            string endpoint = $"https://graph.microsoft.com/v1.0/me/drive/root:/{ filePath }";
            //
            using (var client = new HttpClient())
            {
                using (var request = new HttpRequestMessage(HttpMethod.Get, endpoint))
                {
                    request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", Token.Get().AccessToken);
                    using (Task<HttpResponseMessage> response =  client.SendAsync(request))
                    {
                        response.Wait();
                        if (response.Result.IsSuccessStatusCode)
                        {
                            string stringResult = await response.Result.Content.ReadAsStringAsync();
                            return JsonConvert.DeserializeObject<FileInfo>(stringResult);
                        }
                        else return null;
                    }
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        public async Task<string> CreateSharingLinkForFile(string filePath)
        {
            FileInfo fi = info(filePath).Result;
            //
            string endpoint = $"https://graph.microsoft.com/v1.0/me/drive/items/{ fi.Id }/createLink";
            SharingLinkInfo link = new SharingLinkInfo("view");

            using (var client = new HttpClient())
            {
                using (var request = new HttpRequestMessage(HttpMethod.Post, endpoint))
                {
                    //request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", Token.Get().AccessToken);

                    request.Content = new StringContent(JsonConvert.SerializeObject(link), Encoding.UTF8, "application/json");
                    using (Task<HttpResponseMessage> res = client.SendAsync(request))
                    {
                        res.Wait();
                        HttpResponseMessage response = res.Result;
                        if (response.IsSuccessStatusCode)
                        {
                            string stringResult = await response.Content.ReadAsStringAsync();
                            PermissionInfo permission = JsonConvert.DeserializeObject<PermissionInfo>(stringResult);
                            return permission.Link.WebUrl;
                        }
                        else return "";
                    }
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string SharingLink( string filePath)
        {
            
            try
            {
                return CreateSharingLinkForFile(filePath).Result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}