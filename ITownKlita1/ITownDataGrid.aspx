﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ITownDataGrid.aspx.cs" Inherits="ITownKlita1.ITownDataGrid" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div dir="rtl" style="font-family: Arial, Helvetica, sans-serif; font-size: medium">
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" DataKeyNames="OID" DataSourceID="itownklita" ForeColor="Black" HorizontalAlign="Right">
                <Columns>
                    <asp:CommandField SelectText="בחירה" ShowSelectButton="True" />
                    <asp:BoundField DataField="OID" HeaderText="OID" InsertVisible="False" ReadOnly="True" SortExpression="OID" />
                    <asp:BoundField DataField="check_v" HeaderText="check_v" SortExpression="check_v" />
                    <asp:BoundField DataField="code_mavaat" HeaderText="code_mavaat" SortExpression="code_mavaat" />
                    <asp:BoundField DataField="land_desgnation_plan" HeaderText="land_desgnation_plan" SortExpression="land_desgnation_plan" />
                    <asp:BoundField DataField="code_taba" HeaderText="code_taba" SortExpression="code_taba" />
                    <asp:BoundField DataField="plan_date" DataFormatString="{0:dd/MM/YYYY}" HeaderText="plan_date" SortExpression="plan_date" />
                    <asp:BoundField DataField="lot_num" HeaderText="lot_num" SortExpression="lot_num" />
                    <asp:BoundField DataField="yeshuv_name" HeaderText="yeshuv_name" SortExpression="yeshuv_name" />
                    <asp:BoundField DataField="lot_area" HeaderText="lot_area" SortExpression="lot_area" />
                    <asp:BoundField DataField="uses" HeaderText="uses" SortExpression="uses" />
                    <asp:BoundField DataField="bld_area_above_main" HeaderText="bld_area_above_main" SortExpression="bld_area_above_main" />
                    <asp:BoundField DataField="bld_area_above_service" HeaderText="bld_area_above_service" SortExpression="bld_area_above_service" />
                    <asp:BoundField DataField="bld_area_below_main" HeaderText="bld_area_below_main" SortExpression="bld_area_below_main" />
                    <asp:BoundField DataField="bld_area_below_service" HeaderText="bld_area_below_service" SortExpression="bld_area_below_service" />
                    <asp:BoundField DataField="bld_area_total_m2" HeaderText="bld_area_total_m2" SortExpression="bld_area_total_m2" />
                    <asp:BoundField DataField="bld_area_total_perc" HeaderText="bld_area_total_perc" SortExpression="bld_area_total_perc" />
                    <asp:BoundField DataField="bld_line_frnt" HeaderText="bld_line_frnt" SortExpression="bld_line_frnt" />
                    <asp:BoundField DataField="bld_line_rear" HeaderText="bld_line_rear" SortExpression="bld_line_rear" />
                    <asp:BoundField DataField="bld_line_dirc_side_one" HeaderText="bld_line_dirc_side_one" SortExpression="bld_line_dirc_side_one" />
                    <asp:BoundField DataField="bld_line_dirc_side_two" HeaderText="bld_line_dirc_side_two" SortExpression="bld_line_dirc_side_two" />
                    <asp:BoundField DataField="building_height" HeaderText="building_height" SortExpression="building_height" />
                    <asp:BoundField DataField="floors" HeaderText="floors" SortExpression="floors" />
                    <asp:BoundField DataField="basements" HeaderText="basements" SortExpression="basements" />
                    <asp:BoundField DataField="apartments" HeaderText="apartments" SortExpression="apartments" />
                    <asp:BoundField DataField="density" HeaderText="density" SortExpression="density" />
                    <asp:BoundField DataField="land_cover" HeaderText="land_cover" SortExpression="land_cover" />
                    <asp:BoundField DataField="other_limitations" HeaderText="other_limitations" SortExpression="other_limitations" />
                    <asp:BoundField DataField="another_comment" HeaderText="another_comment" SortExpression="another_comment" />
                </Columns>
                <FooterStyle BackColor="#CCCCCC" />
                <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                <RowStyle BackColor="White" />
                <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#808080" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#383838" />
            </asp:GridView>
            <asp:SqlDataSource ID="itownklita" runat="server" ConnectionString="<%$ ConnectionStrings:ITown_KlitaConnectionString %>" SelectCommand="SELECT * FROM [Sheet1]"></asp:SqlDataSource>
        </div>
    </form>
</body>
</html>
